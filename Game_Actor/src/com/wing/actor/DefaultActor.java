package com.wing.actor;

import com.wing.actor.mailbox.DefaultMailbox;

/**
 * 默认的Actor实现
 * @author 杜祥
 * @create 2017年9月18日
 */
public abstract class DefaultActor extends AbstractActor
{

	public DefaultActor()
	{
		super(new DefaultMailbox());
	}
	
}
