package com.wing.actor.message;

import com.wing.actor.Actor;

/**
 * 默认的消息
 * @author 杜祥
 * @create 2017年9月19日
 */
public class DefaultMessage implements Message
{
	private final Actor sender;
	
	private final Object data;
	
	
	public DefaultMessage(Actor sender, Object data) 
	{
		this.sender = sender;
		this.data = data;
	}
	
	
	@Override
	public Actor getSender()
	{
		return sender;
	}


	@Override
	public Object getData()
	{
		return data;
	}
	
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName() + "[" + bodyString() + "]";
	}


	protected String bodyString() 
	{
		return "sender=" + sender + ", data=" + data;
	}

}
