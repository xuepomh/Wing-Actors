package com.wing.actor.message;

import com.wing.actor.Actor;

/**
 * 消息类，主要负责Actor之间的信息传递
 * @author 杜祥
 * @create 2017年9月8日
 */
public interface Message
{
	
	/**
	 * 消息的发送者
	 * @return
	 */
	Actor getSender();
	
	
	/**
	 * 消息内容
	 * @return
	 */
	Object getData();
	
}
