package com.wing.actor.message;

/**
 * 优先级类型
 * @author 杜祥
 * @create 2017年9月18日
 */
public enum PriorityType
{
	LOW,//低
	MIDDLE,//中
	HIGHT,//高
}
