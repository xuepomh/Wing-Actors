package com.wing.actor.message;

/**
 * 优先级消息
 * @author 杜祥
 * @create 2017年9月18日
 */
public interface PriorityMessage extends Message
{
	/**
	 * 获得消息的优先级
	 * @return
	 */
	public PriorityType getPriority();
}
