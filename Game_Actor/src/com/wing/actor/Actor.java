package com.wing.actor;

import com.wing.actor.message.Message;

/**
 * Actor是一个执行单元，负责消息的接收、处理。
 * @author 杜祥
 * @create 2017年9月8日
 */
public interface Actor
{
	
	/**
	 * 获得Actor的名称，需要保证Actor名称唯一
	 */
	String getName();
	
	
	/**
	 * 设置Actor的名称
	 * @param name
	 */
	void setName(String name);
	
	
	
	/**
	 * 获得Actor的组名称
	 * @return
	 */
	String getGroup();
	
	
	
	/**
	 * 设置Actor的组名称
	 */
	void setGroup(String groupName);
	
	
	
	
	/**
	 * 激活时调用
	 */
	void activate();

	
	/**
	 * 停止时调用
	 */
	void deactivate();
	
	
	
	/**
	 * 是否活跃
	 * @return
	 */
	boolean hasActive();
	

	
	
	/**
	 * 向此Actor发送消息
	 * @param message
	 * @return
	 */
	boolean sendMessage(Message message);
	
	
	
	
	/**
	 * 是否有消息
	 * @return
	 */
	boolean hasMessage();
	
	
	
	/**
	 * 获得消息数量
	 * @return
	 */
	int getMessageCount();
	
	
}
