package com.wing.actor.mailbox;

import com.wing.actor.message.Message;

/**
 * Actor对象的邮箱
 * @author 杜祥
 * @create 2017年9月18日
 */
public interface ActorMailbox
{
	/**
	 * 获取信箱第一个消息
	 * @return
	 */
	public Message poll();
	
	
	/**
	 * 将消息加入信箱的尾部
	 * @param message
	 * @return
	 */
	public void offer(Message message);
	
	
	/**
	 * 获取信箱的长度
	 * @return
	 */
	public int size();
	
	
	
	/**
	 * 信箱是否为空
	 * @return
	 */
	public boolean isEmpty();
	
	
	
	
	/**
	 * 清空信箱
	 */
	public void clear();
}
