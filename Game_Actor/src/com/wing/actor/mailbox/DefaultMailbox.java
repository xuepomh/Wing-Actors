package com.wing.actor.mailbox;

import java.util.ArrayDeque;

import com.wing.actor.message.Message;

/**
 * 默认的信箱
 * @author 杜祥
 * @create 2017年9月18日
 */
public class DefaultMailbox implements ActorMailbox
{
	private final ArrayDeque<Message> mailbox;
	
	
	
	
	public DefaultMailbox()
	{
		this(64);
	}
	
	
	public DefaultMailbox(int minSize) 
	{
		mailbox = new ArrayDeque<Message>(minSize);
	}
	
	
	/**
	 * 获取信箱第一个消息
	 * @return
	 */
	@Override
	public Message poll()
	{
		return mailbox.poll();
	}
	
	
	
	/**
	 * 将消息加入信箱的尾部
	 * @param message
	 * @return
	 */
	@Override
	public void offer(Message message)
	{
		mailbox.offer(message);
	}
	
	
	/**
	 * 获取信箱的长度
	 * @return
	 */
	@Override
	public int size() 
	{
		return mailbox.size();
	}
	
	
	
	/**
	 * 信箱是否为空
	 * @return
	 */
	@Override
	public boolean isEmpty()
	{
		return mailbox.isEmpty();
	}
	
	
	
	
	/**
	 * 清空信箱
	 */
	@Override
	public void clear() 
	{
		mailbox.clear();
	}
}
