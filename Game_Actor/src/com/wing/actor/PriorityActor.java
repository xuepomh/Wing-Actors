package com.wing.actor;

import com.wing.actor.mailbox.PriorityMailbox;


/**
 * 基于优先级实现的Actor
 * @author 杜祥
 * @create 2017年9月18日
 */
public abstract class PriorityActor extends AbstractActor
{

	public PriorityActor()
	{
		super(new PriorityMailbox());
	}
}
