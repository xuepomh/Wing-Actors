package com.wing.actor;



/**
 * 游戏时间管理
 * @author 杜祥
 * @create 2015-4-13
 */
public class GameTimeManager extends WingThread
{
	/**
	 * �?始时�?
	 */
	private final long startTime;
	
	/**
	 * 当前时间
	 */
	private static volatile long currentTime;
	
	
	/**
	 * 计数�?
	 */
	private int count = 0;
	

	
	private GameTimeManager()
	{
		super("time-manager");
		setPriority(Thread.MAX_PRIORITY);
		setDaemon(true);
		startTime = System.currentTimeMillis();
		currentTime = startTime;
		start();
	}
	
	
	@Override
	protected void runTurn()
	{
		if(count == 10)
		{
			currentTime = System.currentTimeMillis();
			count = 0;
		}
		else
		{
			currentTime = currentTime + 1L;
			count += 1;
		}
	}

	@Override
	protected void sleepTurn() throws InterruptedException 
	{
		Thread.sleep(1);
	}

	
	/**
	 * 获得当前的时�?
	 * @return
	 */
	public static long getCurrentTime() 
	{
		return currentTime;
	}
	
	
	public static GameTimeManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}

	
	
	private static class SingletonHolder
	{
		protected final static GameTimeManager INSTANCE = new GameTimeManager();
	}
	
	

}
