package com.wing.actor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wing.actor.message.DefaultMessage;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

/**
 * Actor测试类
 * @author 杜祥
 * @create 2017年9月19日
 */
public class ActorTestMain
{
	private final static Logger log = LoggerFactory.getLogger(ActorTestMain.class);
	

	public static void main(String[] args)
	{
		initializedLog();
		
		GameTimeManager.getInstance();
		
		ActorManager defaultManager = ActorManager.getDefaultInstance();
		
		int maxActorNum = 100000;
		CountDownLatch latch = new CountDownLatch(maxActorNum - 1);
		List<AbstractActor> actors = new ArrayList<AbstractActor>();
		
		for(int i = 0; i < maxActorNum; i++) 
		{
			DefaultActorTest actor = (DefaultActorTest)defaultManager.createAndStartActor(DefaultActorTest.class, "DefaultActorTest:" + i);
			
			actor.setLatch(latch);
			
			actors.add(actor);
		}
		
		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		log.info("开始发送。。。");
		long time = System.currentTimeMillis();
		AbstractActor actor = null;
		for(int i = 0; i < actors.size(); i++) 
		{
			actor = actors.get(i);
			
			for(int j = 0; j < 1000; j++) 
			{
				actor.sendMessage(new DefaultMessage(actor, new MessageData(GameTimeManager.getCurrentTime())));
			}
		}
		
		log.info("发送完成，耗时：" + (System.currentTimeMillis() - time));
		
		try
		{
			latch.await();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		log.info("执行结束，耗时：" + (System.currentTimeMillis() - time));
		
		DefaultActorTest actorTest;
		int count = 0;
		int delayCount = 0;
		int delayTimeCount = 0;
		for(int i = 0; i < actors.size(); i++) 
		{
			actorTest = (DefaultActorTest)actors.get(i);
			
			ActorStatsTest statsTest =  actorTest.getStats();
			
			count += statsTest.getCount();
			delayCount += statsTest.getDelayCount();
			delayTimeCount += statsTest.getDelayTimeCount();
			
			//log.info("actor[" + actorTest.getName() + "]count[" + statsTest.getCount() + 
			//		"]delayCount[" + statsTest.getDelayCount() + 
			//		"]delayTimeCount[" + statsTest.getDelayTimeCount() + "]");
		}
		
		log.info("sumcount[" + count + "]sumdelayCount[" + delayCount + 
				"]sumdelayTimeCount[" + delayTimeCount + "]");
	}

	
	
	/**
	 * 初始化日志服务
	 */
	private final static void initializedLog()
	{
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		try
		{
			JoranConfigurator configurator = new JoranConfigurator();
			configurator.setContext(lc);
			lc.reset();
			configurator.doConfigure("./config/slf4j-logback.xml");
		}
		catch (JoranException e)
		{
			throw new RuntimeException("启动日志服务错误，关闭程序。。。", e);
		}
	}
}
