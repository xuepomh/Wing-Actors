package com.wing.actor;

/**
 * Actor统计
 * @author 杜祥
 * @create 2017年9月19日
 */
public class ActorStatsTest
{
	/**
	 * 数量
	 */
	private int count;
	
	/**
	 * 延迟数量
	 */
	private int delayCount;
	
	
	/**
	 * 延迟的总时间
	 */
	private int delayTimeCount;
	
	
	public int getCount()
	{
		return count;
	}

	
	public void addCount() 
	{
		count += 1;
	}
	


	public int getDelayCount()
	{
		return delayCount;
	}
	
	
	public void addDelayCount()
	{
		delayCount += 1;
	}



	public int getDelayTimeCount()
	{
		return delayTimeCount;
	}

	
	public void addDelayTimeCount(int addValue) 
	{
		delayTimeCount += addValue;
	}
	
	
}
