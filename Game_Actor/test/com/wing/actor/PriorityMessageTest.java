package com.wing.actor;

import com.wing.actor.message.PriorityMessage;
import com.wing.actor.message.PriorityType;

/**
 * 优先级消息测试
 * @author 杜祥
 * @create 2017年9月19日
 */
public class PriorityMessageTest implements PriorityMessage
{

	private final Actor sender;
	
	private final Object data;
	
	private final PriorityType priority;
	
	
	public PriorityMessageTest(Actor sender, PriorityType priority, Object data) 
	{
		this.sender = sender;
		this.priority = priority;
		this.data = data;
	}
	
	
	@Override
	public Actor getSender()
	{
		return sender;
	}


	@Override
	public Object getData()
	{
		return data;
	}
	
	
	@Override
	public PriorityType getPriority()
	{
		return priority;
	}
	
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName() + "[" + bodyString() + "]";
	}


	protected String bodyString() 
	{
		return "sender=" + sender + ", priority=" + priority + ", data=" + data;
	}

	

}
