package com.wing.actor;


/**
 * 与ExecuteThread使用类似�?
 * 但是这个类是继承Thread，可以使用Thread的start()方法来开启线�?,并且提供了关闭线程的方法
 * @author 杜祥
 * @date 2012-10-22
 */
public abstract class WingThread extends Thread
{
	protected WingThread()
	{
		super();
	}

	protected WingThread(String name)
	{
		super(name);
	}
	
	private volatile boolean	_isAlive	= true;
	
	
	public final void shutdown() throws InterruptedException
	{
		onShutdown();

		_isAlive = false;

		join();
	}

	protected void onShutdown()
	{
	}
	
	
	@Override
	public final void run()
	{
		try
		{
			while (_isAlive)
			{
				runTurn();

				try
				{
					sleepTurn();
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
		finally
		{
			onFinally();
		}
	}
	
	protected abstract void runTurn();

	protected abstract void sleepTurn() throws InterruptedException;

	protected void onFinally()
	{
	}
}
