package com.wing.actor;

/**
 * 消息数据
 * @author 杜祥
 * @create 2017年9月19日
 */
public class MessageData
{
	private final long sendTime;

	
	public MessageData(long sendTime) 
	{
		this.sendTime = sendTime;
	}

	public long getSendTime()
	{
		return sendTime;
	}
	
}
