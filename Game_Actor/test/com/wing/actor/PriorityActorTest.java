package com.wing.actor;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wing.actor.message.Message;

/**
 * 优先级
 * @author 杜祥
 * @create 2017年9月19日
 */
public class PriorityActorTest extends PriorityActor
{

	private final static Logger log = LoggerFactory.getLogger(DefaultActorTest.class);

	private ActorStatsTest stats = new ActorStatsTest();
	
	private CountDownLatch latch;
	

	@Override
	protected void onStartup()
	{
	}

	
	@Override
	protected void receive(Message messge)
	{
		MessageData data = (MessageData)messge.getData();
		
		long time = GameTimeManager.getCurrentTime();
		
		int delay = (int) (time - data.getSendTime());
		
		stats.addCount();
		if(delay > 0) 
		{
			stats.addDelayCount();
			stats.addDelayTimeCount(delay);
		}
		
		if(stats.getCount() == 100)
		{
			latch.countDown();
			//log.info("thread[" + Thread.currentThread().getName() + "]actor[" + getName() + "]index[" + stats.getCount() + "]");	
		}

	}


	@Override
	public String toString()
	{
		return getName();
	}


	public ActorStatsTest getStats()
	{
		return stats;
	}


	public CountDownLatch getLatch()
	{
		return latch;
	}


	public void setLatch(CountDownLatch latch)
	{
		this.latch = latch;
	}
	
	

}
